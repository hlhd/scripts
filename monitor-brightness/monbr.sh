#!/usr/bin/bash
# 
# Monitor brightness setter for intel_backlight & acpi_video0 by Hilal Hadyanto
# https://t.me/hianto

source ~/.my_hostnames

# =============================================================================
case "$(hostname)" in
    $my_aspire_hostname)
        brightness=/sys/class/backlight/acpi_video0/brightness
        max_br=/sys/class/backlight/acpi_video0/max_brightness
        ;;
    $my_ideapad_hostname)
        brightness=/sys/class/backlight/intel_backlight/brightness
        max_br=/sys/class/backlight/intel_backlight/max_brightness
        ;;
esac
min_br=3000

# =============================================================================
# cat $brightness

function inc () {
    if [ "$(command -v light)" ]; then
        if [ $(cat $brightness) -eq $(cat $max_br) ]; then
            notify-send "Screen brightness already at maximum value!"
        else
            case "$(hostname)" in
                $my_aspire_hostname)
                    light -s "sysfs/backlight/acpi_video0" -A 2;;
                $my_ideapad_hostname)
                    light -s "sysfs/backlight/intel_backlight" -A 2;;
            esac
        fi
    elif [ "$(command -v xbacklight)" ]; then
        if [ $(cat $brightness) -eq $(cat $max_br) ]; then
            notify-send "Screen brightness already at maximum value!"
        else
            xbacklight -inc 2
        fi
    elif [ "$(command -v brightnessctl)" ]; then
        if [ $(cat $brightness) -eq $(cat $max_br) ]; then
            notify-send "Screen brightness already at maximum value!"
        else
            brightnessctl -q s 2%+
        fi
    else
        if [ $(bc <<< $(cat $brightness)+2000) -lt $(cat $max_br) ]; then
            echo $(bc <<< $(cat $brightness)+2000) > $brightness
        else
            echo $(cat $max_br) > $brightness
            notify-send "Screen brightness raches maximum value"
        fi
    fi
}

function dec () {
    if [ "$(command -v light)" ]; then
        if [ $(cat $brightness) -eq $min_br -o $(cat $brightness) -lt $min_br ]; then
            notify-send "Screen brightness already at minimum value!"
        else
            case "$(hostname)" in
                $my_aspire_hostname)
                    light -s "sysfs/backlight/acpi_video0" -U 2;;
                $my_ideapad_hostname)
                    light -s "sysfs/backlight/intel_backlight" -U 2;;
            esac
        fi
    elif [ "$(command -v xbacklight)" ]; then
        if [ $(cat $brightness) -eq $min_br -o $(cat $brightness) -lt $min_br ]; then
            notify-send "Screen brightness already at minimum value"
        else
            xbacklight -dec 5
        fi
    elif [ "$(command -v brightnessctl)" ]; then
        if [ $(cat $brightness) -eq $min_br -o $(cat $brightness) -lt $min_br ]; then
            notify-send "Screen brightness already at minimum value!"
        else
            brightnessctl -q s 2%-
        fi
    else
        if [ $(bc <<< $(cat $brightness)-2000) -gt $min_br ]; then
            echo $(bc <<< $(cat $brightness)-2000) > $brightness
        else
	    echo $min_br > $brightness
            notify-send "Screen brightness reaches minimum value"
        fi
    fi
}

