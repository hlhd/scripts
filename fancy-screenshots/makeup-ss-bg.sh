#!/usr/bin/env bash
#
# untuk mempercantik screenshot (mirip ss nya macOS)
# bagian dari makeup_screenshot

# =============================================================================
# Call
DIR=$HOME/.local/bin

. ${DIR}/makeup-ss.sh

# =============================================================================
case $1 in
    "g")
        pake_gradien
        notify-send "ss modified (added +shd+gra)";;
    "b")
        pake_bg
        notify-send "ss modified (added +shd+bg)";;
    "br")
        pake_bg_rounded
        notify-send "ss modified (added +round+shd+bg)";;
    "bbr")
        pake_bg_bdr_rounded
        notify-send "ss modified (added +round+bdr+shd+bg)";;
    "gr")
        pake_gradien_rounded
        notify-send "ss modified (added +round+shd+gra)";;
    "gbr")
        pake_gradien_bdr_rounded
        notify-send "ss modified (added +round+bdr+shd+gra)";;
esac

