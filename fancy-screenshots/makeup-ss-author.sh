#!/usr/bin/env bash
#
# untuk mempercantik screenshot (mirip ss nya macOS)
# bagian dari makeup_screenshot

# =============================================================================
# Call
DIR=$HOME/.local/bin

. ${DIR}/makeup-ss.sh

# =============================================================================
case $1 in
    "g")
        pake_gradien_auth
        notify-send "ss modified (added +shd+gra+auth)";;
    "b")
        pake_bg_auth
        notify-send "ss modified (added +shd+bg+auth)";;
    "br")
        pake_bg_auth_rounded
        notify-send "ss modified (added +round+shd+bg+auth)";;
    "bbr")
        pake_bg_bdr_auth_rounded
        notify-send "ss modified (added +round+bdr+shd+bg+auth)";;
    "gr")
        pake_gradien_auth_rounded
        notify-send "ss modified (added +round+shd+gra+auth)";;
    "gbr")
        pake_gradien_bdr_auth_rounded
        notify-send "ss modified (added +round+bdr+shd+gra+auth)";;
esac
