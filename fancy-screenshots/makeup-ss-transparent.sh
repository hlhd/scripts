#!/usr/bin/env bash
#
# untuk mempercantik screenshot (mirip ss nya macOS)
# bagian dari makeup_screenshot

# =============================================================================
# Call
DIR=$HOME/.local/bin

. ${DIR}/makeup-ss.sh

# =============================================================================
case $1 in
    "r")
        pake_rounded
        notify-send "ss modified (added +round)";;
    "br")
        pake_bdr_rounded
        notify-send "ss modified (added +round+border)";;
    "s")
        pake_shadow
        notify-send "ss modified (added +shd)";;
    "sr")
        pake_shadow_rounded
        notify-send "ss modified (added +round+shd)";;
    "sbr")
        pake_shadow_bdr_rounded
        notify-send "ss modified (added +round+border+shd)";;
esac

