#!/usr/bin/env bash
#
# setting the touch pad to suite my need
# useful doc and link
# man xinput
# libinput list-devices
# https://wiki.archlinux.org/index.php/Libinput#Via_xinput
#
# !!!!
# 11 is the id of the device in this case is Synaptics TM2714-002
# the id can be seen with xinput list command
#

# enable tap to click behaviour
xinput set-prop Synaptics\ TM2714-002 "libinput Tapping Enabled" 1

# enable tap drag lock behaviour
xinput set-prop Synaptics\ TM2714-002 "libinput Tapping Drag Lock Enabled" 1

# enable natural scrolling
xinput set-prop Synaptics\ TM2714-002 "libinput Natural Scrolling Enabled" 1
